# Tutorials

```{toctree}
:maxdepth: 1

Basic concpet <tutorials/basic_concept>
Landfill emissions from Solid Waste in Germany <tutorials/swd_Germany>
Consider uncertainty <tutorials/uncertainty>

```
