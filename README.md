# ipcc

The package allows users to access individual equations and sequences of equations as `ipcc.<volume>.<chapter>.elementary.<equation>` or `ipcc.<volume>.<chapter>.sequence.<tier_method>`. The package allows users to access the data as Pandas dataframes in `ipcc.<volume>.<chapter>.dimension.<table>`, `ipcc.<volume>.<chapter>.parameter.<table>` and `ipcc.<volume>.<chapter>.concordance.<table>`. Dimensions list valid coordinates to access parameters; parameters are values to be used in equations; and concordances specify mappings between the different components of a dimension.

The package also allows taking into account uncertainty information. Within a sequence, the user can choose between analytical error propagation and Monte Carlo simulation. Thereby, the values of an equation are transformed into [ufloat](https://uncertainties-python-package.readthedocs.io/en/latest/) or [numpy.array](https://numpy.org/doc/stable/reference/generated/numpy.array.html), respectively.

A comrehensive documentation is available [here](https://bonsamurais.gitlab.io/bonsai/util/ipcc).

## Installation for users (only for project members)

Replace the keyword `tag` by the specific version, e.g., `v0.2.0`.

```bash
pip install git+ssh://git@gitlab.com/bonsamurais/bonsai/util/ipcc.git@tag
```

Change `pip` to `pip3` in Linux. Notice that in either of the above cases the path may change. When in production (not ready yet) installation is:

```bash
pip install bonsai_ipcc
```

## Instalation for contributors

```bash
git clone git@gitlab.com:bonsamurais/bonsai/util/ipcc.git
cd ipcc/
pip install -e .
```

## Basic use

Inside a Python console or notebook.

Create a instance of the IPCC class:

```python
import ipcc
my_ipcc = ipcc.IPCC()
```

With `my_ipcc.<volume>.<chapter>` the ippc package follows the structure of the IPCC guidelines.
To show the elementary equations and sequences of a certain chapter in a specific volume:

```python
dir(my_ipcc.waste.swd.elementary)
# dir(my_ipcc.waste.swd.sequence)
```

To find information about a elementary equation:

```python
help(my_ipcc.waste.swd.elementary.ddoc_from_wd_data)
```

This would print the docstring with information on the parameters and the reqiured units:

```
Help on function ddoc_from_wd_data in module ipcc.waste.swd.elementary:

ddoc_from_wd_data_tier1(waste, doc, doc_f, mcf)
    Equation 3.2 (tier 1)

    Calculates the decomposable doc (ddocm) from waste disposal data.

    Argument
    ---------
    waste (tonnes) : float
        Amount of waste
        (either wet or dry-matter, but attention to doc!)
    doc (kg/kg) : float
        Fraction of degradable organic carbon in waste.
    doc_F (kg/kg) : float
        Fraction of doc that can decompose.
    mcf (kg/kg) : float
        CH4 correction factor for aerobic decomposition in the year of decompostion.

    Returns
    -------
    VALUE: float
        Decomposable doc (tonnes/year)
```

To show the dimensions of a certain parameter:

```python
my_ipcc.waste.swd.parameter.mcf.index.names
```
```
FrozenList(['swds_type', 'property'])
```

To find the possible values of a dimension:

```python
my_ipcc.waste.swd.dimension.swds_type.index
```
```
Index(['managed', 'managed_well_s-a', 'managed_poorly_s-a', 'managed_well_a-a',
       'managed_poorly_a-a', 'unmanaged_deep', 'unmanaged_shallow',
       'uncharacterised'],
      dtype='object', name='code')
```

To retrieve the value and the unit of a certain parameter.
```python
my_ipcc.waste.swd.parameter.mcf.loc[("managed","def")]
```
```
value      1.0
unit     kg/kg
Name: (managed, def), dtype: object
```


### Run a tier sequence

To calculate the GHG inventory based on a tier method, specifiy the keywords of the sequence:

```python
my_tier = my_ipcc.waste.incineration.sequence.tier1_co2(
          year=2010, region="Germany",wastetype="msw_plastics", incintype="inc_unspecified", uncertainty="def")

# show the list of steps of the sequence
my_tier.__dict__
```
For uncertainty calculation based on Monte Carlo use `uncertainty="monte_carlo"`, for analytical error propagation use `uncertainty="analytical"`.

To retrieve the result's value of a sequence's step, type:
```python
my_tier.co2_emissions.value
```

> **_NOTE:_** The type of `value` depends on the uncertainty assessment. For `uncertainty = "def"`: `type = float`, for `uncertainty = "analytical"`: `type = ufloat` and for `uncertainty = "monte-carlo"`: `type = numpy.array`. Furthermore, some tier sequences provide time series instead of one single value. If so, `value` is of type `numpy.array`, including the values for different years. The type of each years' value also depend on the uncertainty assessment.

### Add own data

Data, here in this pacakge, is distinguished between parameter tables, concordance tables and dimension tables.
A parameter table includes the values used by the equations of the IPCC guidelines. The values for these parameters depend on the parameters dimensions, e.g., the value for the dry matter content depends on the dimension **waste type**. For reasons of clarification, these dimension are further specified in dimension tables. Concordance tables are used to specify subsets of attributes (currently only a concordance table for **region** is used, which might be sufficient for the entire package).

Since the IPCC guidelines do not provide values for all parameters, the user is required to collect data and add these to the existing parameter tables.
To do this, use pandas.DataFrame:
```python
import pandas as pd

d = {"property": ["def", "min", "max", "abs_min", "abs_max"],
    "value": [0.3, 0.1, 0.1, 0, 1],
    "unit": ["kg/kg", "kg/kg", "kg/kg", "kg/kg", "kg/kg"]
    }


df = pd.DataFrame(d).set_index("property")

swd.parameter.r = df
```

> **_NOTE:_** When adding own data, the user is encouraged to also specify uncertainty information. Property "def" is always required and specifies the mean value. For uncertainty analysis "min", "max", "abs_min" and "abs_max" are required ("min": 2.5 percentile, "max": 97.5 percentile, "abs_min": absolute minimum, "abs_max": absolute maximum).

To get a list of all parameters involved in a sequence, you can do:
```python
my_ipcc.inspect(my_ipcc.waste.swd.sequence.tier1_ch4)
```












