from ..._data import Concordance, Dimension, Parameter

dimension = Dimension("data/")

parameter = Parameter(["data/industry/mineral"])

concordance = Concordance("data/")
