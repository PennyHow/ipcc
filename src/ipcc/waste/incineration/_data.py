from ..._data import Concordance, Dimension, Parameter

dimension = Dimension("data/")

parameter = Parameter(["data/waste/incineration/", "data/waste/waste_generation/"])

concordance = Concordance("data/")
