from ..._data import Concordance, Dimension, Parameter

dimension = Dimension("data/")

parameter = Parameter(["data/waste/wastewater/", "data/waste/waste_generation/"])

concordance = Concordance("data/")
