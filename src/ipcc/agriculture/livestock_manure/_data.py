from ..._data import Concordance, Dimension, Parameter

dimension = Dimension("data/")

parameter = Parameter(["data/agriculture/livestock_manure", "data/waste/swd"])

concordance = Concordance("data/")
