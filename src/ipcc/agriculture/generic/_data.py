from ..._data import Concordance, Dimension, Parameter

dimension = Dimension("data/")

parameter = Parameter(["data/agriculture/generic"])

concordance = Concordance("data/")
