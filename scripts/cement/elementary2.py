# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 15:13:53 2023

@author: mathi
"""

""""
This script calculates the emissions from cement production (calcination only, not fuel
combustion!), based on the 2019 Refinement to the 2006 IPCC Guidelines for National
Greenhouse Gas Inventories, volume 3, chapter 2.
The equations in this script are coming only from the IPCC guidelines.
"""


def caco3_supply(f_CaO_on_clinker, f_molec_weight_CaO_on_CaCO3):
    """
    This function calculates the CaCO3 (Calcium carbonate) needed to produce 1 tonne of
    clinker. This value is dependent on the mass fraction of CaO (calcium oxide or lime)
    on clinker: f_CaO_on_clinker (can vary but a default value is 0.65) and on the
    relative molecular weight of CaO on CaCO3.
    IPCC guidelines (Volume 3, Chapter 2) do not provide a particular equation, they
    describe the calculations with default value just before Equation 2.4 (Equation
    2.1.1 in the Technical documentation for cement).

    Parameters
    ----------
    f_CaO_on_clinker : float
        Mass fraction of CaO on clinker (that can be defined by design).
        The default value from IPCC is 0.65.
    f_molec_weight_CaO_on_CaCO3 : float
        Molecular weight fraction of CaO on CaCO3.

    Returns
    -------
    CaCO3_supply : float
        Mass of CaCO3 needed to produce 1 tonne of clinker with the particular fraction
        of CaO, in tonne.

    """

    CaCO3_supply = f_CaO_on_clinker / f_molec_weight_CaO_on_CaCO3
    return CaCO3_supply


def calcination_factor_co2(CaCO3_supply, f_molec_weight_CO2_on_CaCO3):
    """
    This function calculates the CO2 emissions stemming from the calcination of CaCO3
    needed to produce 1 tonne of clinker.
    IPCC guidelines (Volume 3, Chapter 2) do not provide a particular equation, they
    describe the calculations with default value just before Equation 2.4 (Equation
    2.1.2 in the Technical documentation for cement).

    Parameters
    ----------
    CaCO3_supply : float
        Mass value of CaCO3 that is calcinated to produce 1 tonne of clinker, in
        tonnes CaCO3/tonne clinker.
    f_molec_weight_CO2_on_CaCO3 : float
        Molecular weight fraction of CO2 on CaCO3.

    Returns
    -------
    CO2_release_calcination_CaCO3 : float
        Mass of CO2 released from the CaCO3 calcination, in tonnes CO2/tonne of clinker.

    """

    CO2_release_calcination_CaCO3 = CaCO3_supply * f_molec_weight_CO2_on_CaCO3
    return CO2_release_calcination_CaCO3


def corrected_calcination_factor_co2(CO2_release_calcination_CaCO3, correc_fact_CKD):
    """
    This function calculates the corrected emission factor for clinker production, in
    tonnes CO2/tonne of clinker.
    Equation 2.4 in the IPCC guidelines (Volume 3, Chapter 2).

    Parameters
    ----------
    CO2_release_calcination_CaCO3 : float
        Mass of CO2 released from the CaCO3 calcination, in tonnes CO2/tonne of clinker.
    correc_fact_CKD : float
        Correction factor due to Cement Kiln Dust (limiting the efficiency of
        calcination). A default assumption from IPCC would be an additional 2% (1.02)
        but can be adjusted.
        The default value from IPCC is 1.02.

    Returns
    -------
    emiss_fact_clinker : float
    Corrected emission factor for clinker producion (in tonnes CO2/tonne of clinker).

    """
    emiss_fact_clinker = CO2_release_calcination_CaCO3 * correc_fact_CKD
    return emiss_fact_clinker


def calcination_co2(
    mass_cement_production,
    f_clinker_on_cement,
    import_clinker,
    export_clinker,
    emiss_fact_clinker,
):
    """
    This function calculates the CO2 emissions stemming from the calcination for a
    certain amount of cement produced.
    Equation 2.1 in the IPCC guidelines (Volume 3, Chapter 2).

    Parameters
    ----------
    mass_cement_production (in tonnes): float
        Total mass of cement produced.
    f_clinker_on_cement (dimensionless): float
        Fraction of clinker on cement.
    clinker_imported (in tonnes): float
        import_clinker of clinker.
    export_clinker (in tonnes): float
        Export of clinker.
    emiss_fact_clinker (in tonnes CO2/tonne of clinker): float
        Emission factor of clinker.

    Returns
    -------
    co2_emitted_cement_calcination: float
        Total CO2 emissions generated from cement production (in tonnes CO2).

    """
    co2_emitted_cement_calcination = (
        mass_cement_production * f_clinker_on_cement - import_clinker + export_clinker
    ) * emiss_fact_clinker
    return co2_emitted_cement_calcination
